# 텍스트 데이터에 대한 감정 분석 <sup>[1](#footnote_1)</sup>

> <font size="3">다양한 기법을 활용하여 텍스트 데이터에 대한 감정 분석을 수행하는 방법을 알아본다.</font>

## 목차

1. [개요](./sentiment-analysis.md#intro)
1. [감정 분석이란?](./sentiment-analysis.md#sec_02)
1. [감정 분석이 중요한 이유?](./sentiment-analysis.md#sec_03)
1. [감정 분석을 수행하는 방법](./sentiment-analysis.md#sec_04)
1. [감정 분석 기법과 도구](./sentiment-analysis.md#sec_05)
1. [감정 분석 어플리케이션과 사례](./sentiment-analysis.md#sec_06)
1. [감정 분석의 과제와 한계](./sentiment-analysis.md#sec_07)
1. [마치며](./sentiment-analysis.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 26 — Sentiment Analysis for Text Data](https://ai.plainenglish.io/sentiment-analysis-for-text-data-ae469a212923?sk=dfb2693296f7eb84785ce3649ce06e2f)를 편역하였습니다.
